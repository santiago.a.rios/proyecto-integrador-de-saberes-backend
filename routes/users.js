var express = require('express');
var router = express.Router();
//const { body, validationResult } = require('express-validator');
//Rol
const RolController = require("../controls/RolController");
var rolController = new RolController();


//
//let jwt = require('jsonwebtoken');


//middleWare: Es un filtrador, filtra las peticiones para si lo que se dice es verdad
//autentificacion
// var auth =  function middleWare(req, res, next){ //next: Dice si pasa el filtro, sigue a la accion siguiente, o sino te quita    ahi
// const token = req.headers['x-api-token']; //Decirle al usuario como debe llamarlo
// console.log(req.headers);

// if (token) {
//   require('dotenv').config();
//   Se obtiene la llave del archivo .env
//   const llave = process.env.KEY; 
//   jwt.verify(token, llave, async (err, decoded) =>{
//     if (err) {
//       res.status(401);  
//       res.json({msg: "Token no válido o expirado!", code: 401});

//     } else {
//       var models = require('../models');
//       var cuenta = models.cuenta;
//       req.decoded = decoded;
//       let aux = await cuenta.findOne({where: {external_id: req.decoded.external}});
//       if (aux == null) {
//         res.status(401);
//       res.json({msg: "Token no válido!", code: 401});
//       } else {
//         next();
//       }
//     }

//   });
// } else {
//   res.status(401);
//   res.json({msg: "No existe token!", code: 401});
  
// }

// }



/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});


router.get('/roles', rolController.listar);





/*router.get('/sumar/:a/:b', function(req, res, next) {
  var a = req.params.a * 1;
  var b = Number(req.params.b);
  var c = a +b;
  res.status(200);
  res.json({"msg":"OK","resp":c});
});
*/
router.post('/sumar', function (req, res, next) {
  var a = Number(req.body.a);
  var b = Number(req.body.b);
  if (isNaN(a) || isNaN(b)) {
    res.status(400);
    res.json({ "msg": "Faltan Datos" });
  }
  console.log(b);
  var c = a + b;
  res.status(200);
  res.json({ "msg": "OK", "resp": c });
});

module.exports = router;
